<?php

namespace snkeng\sources;

use Composer\Package\PackageInterface;
use Composer\Installer\LibraryInstaller;

class CoreInstaller extends LibraryInstaller
{
	/**
	 * {@inheritDoc}
	 */
	public function supports($packageType)
	{
		return 'snkeng-core' === $packageType;
	}

	/**
	 * {@inheritDoc}
	 */
	public function getInstallPath(PackageInterface $package)
	{
		// should be root folder
		return 'snkeng-core-temp/';
	}

}