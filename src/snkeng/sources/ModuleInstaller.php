<?php

namespace snkeng\sources;

use Composer\Package\PackageInterface;
use Composer\Installer\LibraryInstaller;

class ModuleInstaller extends LibraryInstaller
{
	/**
	 * {@inheritDoc}
	 */
	public function supports($packageType)
	{
		return 'snkeng-module' === $packageType;
	}

	/**
	 * {@inheritDoc}
	 */
	public function getInstallPath(PackageInterface $package)
	{
		$prefix = substr($package->getPrettyName(), 0, 14);
		if ('snkeng/module-' !== $prefix) {
			throw new \InvalidArgumentException(
				'Unable to install module, snkeng module '
				.'should always start their package name with '
				.'"snkeng/module-"'
			);
		}

		//
		return 'server_root/snkeng/site_modules/'.substr($package->getPrettyName(), 14);
	}

}