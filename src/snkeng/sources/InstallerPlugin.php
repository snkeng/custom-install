<?php
//
namespace snkeng\sources;

//
use Composer\Composer;
use Composer\IO\IOInterface;
use Composer\Plugin\PluginInterface;

//
class InstallerPlugin implements PluginInterface
{

	public function activate(Composer $composer, IOInterface $io)
	{
		$coreInstaller = new CoreInstaller($io, $composer);
		$composer->getInstallationManager()->addInstaller($coreInstaller);
		$moduleInstaller = new ModuleInstaller($io, $composer);
		$composer->getInstallationManager()->addInstaller($moduleInstaller);
	}

	public function deactivate(Composer $composer, IOInterface $io)
	{
		return [];
	}

	public function uninstall(Composer $composer, IOInterface $io)
	{
		return [];
	}
}
//
