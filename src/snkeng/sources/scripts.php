<?php

namespace snkeng\sources;


use Composer\Script\Event;


//
class scripts
{
	public static function coreRelocation(Event $event)
	{
		print "SNKENG Core relocation start:\n";

		//
		$projectRootPath = realpath(dirname(\Composer\Factory::getComposerFile()));
		print "PROJECT DIRECTORY:\n{$projectRootPath}\n";

		//
		$projectServerRootPath = $projectRootPath . DIRECTORY_SEPARATOR . 'server_root';
		print "PROJECT SERVER ROOT DIRECTORY:\n{$projectServerRootPath}\n";

		$tempSnkengCoreLocation = $projectRootPath . DIRECTORY_SEPARATOR . 'snkeng-core-temp';
		print "TEMP SNK CORE LOCATION:\n{$tempSnkengCoreLocation}\n";

		//
		$tempSnkengCoreSrcLocation = $tempSnkengCoreLocation . DIRECTORY_SEPARATOR . 'src';

		// Directory operations
		self::hard_copy($tempSnkengCoreSrcLocation, $projectServerRootPath);
		self::soft_copy($tempSnkengCoreSrcLocation, $projectServerRootPath);

		// Remove the downloaded folder
		self::recurse_delete($tempSnkengCoreLocation);

		//
		print "SNKENG Core Set.\n";
	}

	// Hard copy (delete->copy) only snkeng core
	private static function hard_copy(string $src, string $dest) : void {
		$src.= DIRECTORY_SEPARATOR . 'snkeng';
		$dest.= DIRECTORY_SEPARATOR . 'snkeng';

		print "Initializing hard copy: ({$src})\n";

		$dirList = scandir($src);

		if ( !$dirList ) {
			throw new \InvalidArgumentException(
				'Src directory empty'
			);
		}

		//
		foreach ( $dirList as $cDir ) {
			if ( in_array($cDir, ['.', '..']) ) {
				continue;
			}

			print "DIR: snkeng/{$cDir}\n";

			//
			$tempFolder = $src . DIRECTORY_SEPARATOR . $cDir;
			$projectFolder = $dest . DIRECTORY_SEPARATOR . $cDir;

			// Delete current files if any (as in update)
			if ( is_dir($projectFolder) ) {
				print "PREVIOUS CORE FOUND, REMOVING.\n";
				self::recurse_delete($projectFolder);
			}

			// Copy contents
			self::recurse_copy($tempFolder, $projectFolder);
		}
	}

	// Soft copy (only copy, if not exists)
	private static function soft_copy(string $src, string $dest) : void {
		print "Initializing soft copy: ({$src})\n";

		//
		foreach ( scandir($src) as $cDir ) {
			if ( in_array($cDir, ['.', '..'], 'snkeng') ) {
				continue;
			}

			//
			$projectFolder = $dest . DIRECTORY_SEPARATOR . $cDir;
			$tempFolder = $src . DIRECTORY_SEPARATOR . $cDir;

			// Check if target copy exists
			if ( !is_dir($tempFolder) ) {
				throw new \InvalidArgumentException(
					'Original folder not found'
				);
			}

			// Copy contents
			self::recurse_safe_copy($tempFolder, $projectFolder);
		}
	}

	//
	public static function recurse_delete(string $dir) : bool | null
	{
		if ( !file_exists($dir) ) {
			return true;
		}

		// Delete file
		if ( !is_dir($dir) ) {
			return unlink($dir);
		}

		// Get elements of folder
		foreach ( scandir($dir) as $item ) {
			if ( in_array($item, ['.', '..']) ) {
				continue;
			}

			if ( !self::recurse_delete($dir . DIRECTORY_SEPARATOR . $item) ) {
				return false;
			}

		}

		return rmdir($dir);
	}

	//
	public static function recurse_copy(string $src, string $dst) : void
	{
		$dir = opendir($src);
		@mkdir($dst, 0775, true);

		if ( !is_dir($dst) ) {
			throw new \InvalidArgumentException(
				'New folder was not created: ' . $dst
			);
		}

		//
		while ( false !== ($file = readdir($dir)) ) {
			// Skip folder navigations
			if ( $file === '.' || $file === '..' ) {
				continue;
			}

			//
			$file_original = $src . DIRECTORY_SEPARATOR . $file;
			$file_target = $dst . DIRECTORY_SEPARATOR . $file;

			// Check if it is a folder or file
			if ( is_dir($file_original) ) {
				self::recurse_copy($file_original, $file_target);
			} else {
				copy($file_original, $file_target);
			}
		}
		closedir($dir);
	}

	//
	public static function recurse_safe_copy(string $src, string $dst) : void
	{
		$dir = opendir($src);
		@mkdir($dst, 0775, true);

		if ( !is_dir($dst) ) {
			throw new \InvalidArgumentException(
				'New folder was not created: ' . $dst
			);
		}

		//
		while ( false !== ($file = readdir($dir)) ) {
			// Skip folder navigations
			if ( $file === '.' || $file === '..' ) {
				continue;
			}
			
			//
			$file_original = $src . DIRECTORY_SEPARATOR . $file;
			$file_target = $dst . DIRECTORY_SEPARATOR . $file;

			// Check if it is a folder or file
			if ( is_dir($file_original) ) {
				self::recurse_safe_copy($file_original, $file_target);
			} else {
				$targetFile = $file_target;
				if ( !file_exists($targetFile) ) {
					copy($file_original, $targetFile);
				}
			}
		}
		closedir($dir);
	}
}