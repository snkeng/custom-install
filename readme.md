Auxiliary composer plugin designed to make the snkeng core available and updatable in a project.

Should be allowed in plugins, and it exists preconfigured in the starting-template.

For more information visit: https://snako.dev.

